/*
 * Ptet_xFP_FlModel.h
 *      Author: ETH iGEM 2016
 */

#ifndef PTET_xFP_FLMODEL_H_
#define PTET_xFP_FLMODEL_H_

#include "ModelFl.h"

#include <stddef.h>

namespace INSIGHTv3 {

class Ptet_xFP_FlModel: public INSIGHTv3::ModelFl {
public:
	Ptet_xFP_FlModel(
			const std::vector<EiVector> background_fluorescence, RngPtr r,
			std::vector<int> output_states);
	virtual ~Ptet_xFP_FlModel();
	void initializeTrajectories(const parameters& params);
	void initializeMeasurement(const parameters& params);

protected:
	void _updatePropensities(const state& X, const parameters& params,
			propensities* props) const;
	void _updateState(const size_t reaction_index, EiVectorRef X) const;
	void _createOutputState(const state& X, const parameters& params,
			EiVectorRef output);
	void _setInitialConditions(EiVectorRef X);

private:

	static const size_t _NUM_SPECIES = 2;
	static const size_t _NUM_PARAMETERS = 7;
	static const size_t _NUM_REACTIONS = 4;
	static const MODELS _MODEL = MODEL_PTET_XFP_FL;
	static const double NP = 15;

	// DistNormal* _dist_normal;
	DistUniform _dist_uniform;
	double _aTc;
	double _prevDose;
};

} /* namespace INSIGHTv3 */

#endif /* PTET_xFP_FLMODEL_H_ */
