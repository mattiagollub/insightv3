/*
 * Esar_GFP_FlModel.h
 *
 *  Created on: 23 sept. 2016
 *      Author: ETH iGEM 2016
 */

#ifndef MODELS_ESAR_GFP_FLMODEL_H_
#define MODELS_ESAR_GFP_FLMODEL_H_


#include "ModelFl.h"

#include <stddef.h>

namespace INSIGHTv3 {

class Esar_GFP_FlModel: public INSIGHTv3::ModelFl {
public:
	Esar_GFP_FlModel(
			const std::vector<EiVector> background_fluorescence, RngPtr r,
			std::vector<int> output_states);
	virtual ~Esar_GFP_FlModel();
	void initializeTrajectories(const parameters& params) {};
	void initializeMeasurement(const parameters& params) {};

protected:
	void _updatePropensities(const state& X, const parameters& params,
			propensities* props) const;
	void _updateState(const size_t reaction_index, EiVectorRef X) const;
	void _createOutputState(const state& X, const parameters& params,
			EiVectorRef output);
	void _setInitialConditions(EiVectorRef X);

private:

	static const size_t _NUM_SPECIES = 9;
	static const size_t _NUM_PARAMETERS = 14;
	static const size_t _NUM_REACTIONS = 17;
	static const MODELS _MODEL = MODEL_ESAR_GFP_FL;
	static const double NP = 15;

	DistPoisson _dist_poisson;

};

} /* namespace INSIGHTv3 */





#endif /* MODELS_ESAR_GFP_FLMODEL_H_ */
