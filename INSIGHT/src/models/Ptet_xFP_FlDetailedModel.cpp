/*
 * Ptet_xFP_FlDetailedModel.h
 *      Author: ETH iGEM 2016
 */

#include <Ptet_xFP_FlDetailedModel.h>
#include <string>
#include <cmath>

#include "INSIGHTv3.h"

namespace INSIGHTv3 {

Ptet_xFP_FlDetailedModel::Ptet_xFP_FlDetailedModel(
		const std::vector<EiVector> background_fluorescence, RngPtr r,
		std::vector<int> output_states) :
		ModelFl(background_fluorescence, r, _NUM_SPECIES,
				_NUM_PARAMETERS + 2 * output_states.size(), _NUM_REACTIONS,
				"aTc-inducible fluorescence proteins detailed model for the ETH iGEM 2016 project",
				_MODEL, output_states), _dist_uniform(*r) {

}

Ptet_xFP_FlDetailedModel::~Ptet_xFP_FlDetailedModel() {
}

#define MRNA_XFP 0
#define XFP 1

void Ptet_xFP_FlDetailedModel::_updatePropensities(const state& X,
		const parameters& params, propensities* props) const {

	// Recover species from X vector
	double mRNAxfp = X(MRNA_XFP);
	double xFP = X(XFP);

	// Recover parameters from params vector
	int begin_index = 2 * output_states.size(); // the first parameters are reserved for the mean and std of the fluourescence
	double l_Ptet = params(begin_index);
	double n_TetR = params(begin_index + 1);
	double Km_TetR = params(begin_index + 2);
	double k_mRNAxfp = params(begin_index + 3);
	double d_mRNAxfp = params(begin_index + 4);
	double k_xFP = params(begin_index + 5);
	double d_xFP = params(begin_index + 6);
	double Ka = params(begin_index + 7);
	double TetR_tot = params(begin_index + 8);

	// Evaluate the propensities
	double aTc = dose;
	double TetR = TetR_tot / (aTc*Ka + 1);
	double P_ON = l_Ptet + (1-l_Ptet)/(pow(Km_TetR, n_TetR)+pow(TetR, n_TetR));
	(*props)(0) = k_mRNAxfp*P_ON*NP;	// P_ON -> P_ON + mRNAxfp
	(*props)(1) = d_mRNAxfp*mRNAxfp;	// mRNAxfp ->
	(*props)(2) = k_xFP*mRNAxfp;		// mRNAxfp -> mRNAxfp + xFP
	(*props)(3) = d_xFP*xFP;			// xFP ->
}

void Ptet_xFP_FlDetailedModel::_updateState(const size_t reaction_index,
		EiVectorRef X) const {

	// Update the state vector according to which reaction fired
	switch (reaction_index) {
	case 0:		// P_ON -> P_ON + mRNAxfp
		X(MRNA_XFP) = X(MRNA_XFP) + 1;
		break;
	case 1:		// mRNAxfp ->
		X(MRNA_XFP) = X(MRNA_XFP) - 1;
		break;
	case 2:		// mRNAxfp -> mRNAxfp + xFP
		X(XFP) = X(XFP) + 1;
		break;
	case 3:		// xFP ->
		X(XFP) = X(XFP) - 1;
		break;
	}
}

void Ptet_xFP_FlDetailedModel::_createOutputState(const state& X,
		const parameters& params, EiVectorRef output) {

	EiConstVectorRef mu = params.head(output_states.size());
	EiConstVectorRef std = params.block(output_states.size(), 0,
			output_states.size(), 1);
	state s = X.replicate(1, 1);
	if (_dist_uniform() < 0.07)
		s(XFP) = 0; // Dead cell
	_addFlLevel(s, mu, std, output);
}

void Ptet_xFP_FlDetailedModel::_setInitialConditions(EiVectorRef X) {
	X(MRNA_XFP) = 0;
	X(XFP) = 0;
	// X(TETR_TOT) = std::floor(std::max(_dist_normal->operator ()(), 0.0));
	// std::cout << "New trajectory with TetR_tot=" << X(TETR_TOT) << "\n";
}

void Ptet_xFP_FlDetailedModel::initializeTrajectories(const parameters& params) {
	// int begin_index = 2 * output_states.size();
	// double mu_TetR = params(begin_index + 8);
	// double std_TetR = params(begin_index + 9);

	// if (_dist_normal != 0)
	//	delete _dist_normal;
	// _dist_normal = new DistNormal(*_r, NormalMapping(mu_TetR, std_TetR));
	//_dist_normal = new DistNormal(*_r, NormalMapping(mu_TetR, std_TetR));
}

}/* namespace INSIGHTv3 */
