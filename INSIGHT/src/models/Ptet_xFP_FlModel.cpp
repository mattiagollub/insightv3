/*
 * Ptet_xFP_FlModel.h
 *      Author: ETH iGEM 2016
 */

#include <Ptet_xFP_FlModel.h>
#include <string>
#include <cmath>

#include "INSIGHTv3.h"

namespace INSIGHTv3 {

Ptet_xFP_FlModel::Ptet_xFP_FlModel(
		const std::vector<EiVector> background_fluorescence, RngPtr r,
		std::vector<int> output_states) :
		ModelFl(background_fluorescence, r, _NUM_SPECIES,
				_NUM_PARAMETERS + 2 * output_states.size(), _NUM_REACTIONS,
				"aTc-inducible fluorescence proteins model for the ETH iGEM 2016 project.",
				_MODEL, output_states), _dist_uniform(*r), _prevDose(-1.0) {

}

Ptet_xFP_FlModel::~Ptet_xFP_FlModel() {
}

#define MRNA_XFP 0
#define XFP 1
// #define KM_ATC 2

void Ptet_xFP_FlModel::_updatePropensities(const state& X,
		const parameters& params, propensities* props) const {

	// Recover species from X vector
	double mRNAxfp = X(MRNA_XFP);
	double xFP = X(XFP);

	// Recover parameters from params vector
	int begin_index = 2 * output_states.size(); // the first parameters are reserved for the mean and std of the fluourescence
	double l_Ptet = params(begin_index);
	double n_aTc = params(begin_index + 1);
	double Km_aTc = std::pow(10.0, params(begin_index + 2) - 10.0);
	double k_mRNAxfp = std::pow(10.0, params(begin_index + 3) - 10.0);
	double d_mRNAxfp = std::pow(10.0, params(begin_index + 4) - 10.0);
	double k_xFP = std::pow(10.0, params(begin_index + 5) - 10.0);
	double d_xFP = std::pow(10.0, params(begin_index + 6) - 10.0);

	// Evaluate the propensities
	double aTc = _aTc;
	double P_ON = l_Ptet + (1-l_Ptet)*pow(aTc, n_aTc)/(Km_aTc+pow(aTc, n_aTc));
	(*props)(0) = k_mRNAxfp*P_ON*NP;	// P_ON -> P_ON + mRNAxfp
	(*props)(1) = d_mRNAxfp*mRNAxfp;	// mRNAxfp ->
	(*props)(2) = k_xFP*mRNAxfp;		// mRNAxfp -> mRNAxfp + xFP
	(*props)(3) = d_xFP*xFP;			// xFP ->
}

void Ptet_xFP_FlModel::_updateState(const size_t reaction_index,
		EiVectorRef X) const {

	// Update the state vector according to which reaction fired
	switch (reaction_index) {
	case 0:		// P_ON -> P_ON + mRNAxfp
		X(MRNA_XFP) = X(MRNA_XFP) + 1;
		break;
	case 1:		// mRNAxfp ->
		X(MRNA_XFP) = X(MRNA_XFP) - 1;
		break;
	case 2:		// mRNAxfp -> mRNAxfp + xFP
		X(XFP) = X(XFP) + 1;
		break;
	case 3:		// xFP ->
		X(XFP) = X(XFP) - 1;
		break;
	}
}

void Ptet_xFP_FlModel::_createOutputState(const state& X,
		const parameters& params, EiVectorRef output) {

	EiConstVectorRef mu = params.head(output_states.size());
	EiConstVectorRef std = params.block(output_states.size(), 0,
			output_states.size(), 1);
	state s = X.replicate(1, 1);
	if (_dist_uniform() < 0.07)
		s(XFP) = 0; // Dead cell
	_addFlLevel(s, mu, std, output);
}

void Ptet_xFP_FlModel::_setInitialConditions(EiVectorRef X) {
	X(MRNA_XFP) = 0;
	X(XFP) = 0;
	/*X(KM_ATC) = std::max(_dist_normal->operator ()(), 0.0);
	std::cout << "New trajectory with Km_aTc=" << X(KM_ATC) << "\n";*/
}

void Ptet_xFP_FlModel::initializeTrajectories(const parameters& params) {

}

void Ptet_xFP_FlModel::initializeMeasurement(const parameters& params) {
	if (_prevDose != dose) {
		if (dose <= 0.0) {
			this->_aTc = 0.0;
		}
		else if (dose > 50) {
			this->_aTc = dose;
		}
		else {
			DistPoisson dist_poisson(*_r, PoissonMapping(dose));
			this->_aTc = dist_poisson();
		}
		_prevDose = dose;
	}
}

}/* namespace INSIGHTv3 */
