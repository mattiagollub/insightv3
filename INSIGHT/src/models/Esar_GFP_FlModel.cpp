/*
/*
 * Esar_GFP_FlModel.cpp
 *      Author: ETH iGEM 2016
 */

#include <Esar_GFP_FlModel.h>
#include <string>
#include <cmath>

#include "INSIGHTv3.h"

namespace INSIGHTv3 {

Esar_GFP_FlModel::Esar_GFP_FlModel(
		const std::vector<EiVector> background_fluorescence, RngPtr r,
		std::vector<int> output_states) :
		ModelFl(background_fluorescence, r, _NUM_SPECIES,
				_NUM_PARAMETERS + 2 * output_states.size(), _NUM_REACTIONS,
				"AHL inducible esabox for the and gate model for the ETH iGEM 2016 project.",
				_MODEL, output_states), _dist_poisson(*_r, PoissonMapping(4)) {

}

Esar_GFP_FlModel::~Esar_GFP_FlModel() {
}
#define ESAR 0
#define DESAR 1
#define DESAR_AHL1 2
#define DESAR_AHL 3
#define PDESAR 4
#define PDESAR_AHL 5
#define PFREE 6
#define MRNA_GFP 7
#define GFP 8


void Esar_GFP_FlModel::_updatePropensities(const state& X,
		const parameters& params, propensities* props) const {


	// Parameters inferred from the GFP experiment
	/*double k_mRNAsfgfp = 0.01;
	double d_mRNAsfgfp = 0.01;
	double k_sfGFP = 0.01;
	double d_sfGFP = 0.01;*/

	// Recover species from X vector
	double Esar = X(ESAR);
	double DEsar = X(DESAR);
	double DEsar_AHL1 = X(DESAR_AHL1);
	double DEsar_AHL = X(DESAR_AHL);
	double Pdesar = X(PDESAR);
	double Pdesar_AHL = X(PDESAR_AHL);
	double Pfree = X(PFREE);
	double mRNAgfp = X(MRNA_GFP);
	double gfp = X(GFP);

	// Recover parameters from params vector
	int begin_index = 2 * output_states.size(); // the first parameters are reserved for the mean and std of the fluourescence
	double k_dimer = params(begin_index);
	double k_dissoc = params(begin_index + 1);
	double k_desarahl1 = params(begin_index + 2);
	double k_desarahl1_ = params(begin_index + 3);
	double k_desarahl2 = params(begin_index + 4);
	double k_desarahl2_ = params(begin_index + 5);
	double k_leak = params(begin_index + 6);
	double k_esarprod = params(begin_index + 7);
	double d_esar = params(begin_index + 8);
	double d_desar = params(begin_index + 9);

	double k_mRNAsfgfp = params(begin_index + 10);
    double d_mRNAsfgfp = params(begin_index + 11);
	double k_sfGFP = params(begin_index + 12);
	double d_sfGFP = params(begin_index + 13);

	// Evaluate the propensities
	double AHL = dose;
	(*props)(0) = k_dimer*Esar*(Esar-1);	// EsaR + Esar -> DEsar
	(*props)(1) = k_dissoc*DEsar;	// EsaR + Esar <- DEsar
	(*props)(2) = k_desarahl1*AHL*DEsar;  //AHL + DEsar -> DEsar_AHL1
	(*props)(3) = k_desarahl1_*DEsar_AHL1;	//AHL + DEsar <- DEsar_AHL1
	(*props)(4) = k_desarahl1*DEsar_AHL1*AHL;  //AHL + DEsar_AHL1 -> DEsar_AHL
	(*props)(5) = k_desarahl1_*DEsar_AHL;  //AHL + DEsar_AHL1 <- DEsar_AHL
	(*props)(6) = k_desarahl2_*Pfree*DEsar_AHL; //DEsar_AHL + Pfree -> Pesar_AHL + AHL
	(*props)(7) = k_desarahl2*Pdesar_AHL*AHL;  //DEsar_AHL + Pfree <- Pesar_AHL + AHL
	(*props)(8) = k_desarahl2_*Pdesar_AHL;  //Pdesar_AHL -> Pesar + AHL
	(*props)(9) = k_desarahl2*Pdesar*AHL; //Pdesar_AHL <- Pesar + AHL
	(*props)(10) = (k_esarprod*k_leak)+(k_esarprod*(1-k_leak)*NP); //-> Esar
	(*props)(11) = d_esar*Esar; //EsaR ->
	(*props)(12) = d_desar*DEsar;  //DEsaR ->
	(*props)(13) = k_mRNAsfgfp*Pfree*NP; //Pfree -> Pfree + mRNAsfgfp
	(*props)(14) = d_mRNAsfgfp*mRNAgfp; //mRNAsfgfp ->
	(*props)(15) = k_sfGFP*mRNAgfp; //mRNAsfgfp -> mRNAsfgfp + sfGFP
	(*props)(16) =  d_sfGFP*gfp;  //sfGFP ->
}

void Esar_GFP_FlModel::_updateState(const size_t reaction_index,
		EiVectorRef X) const {

	// Update the state vector according to which reaction fired
	switch (reaction_index) {
	case 0:		// EsaR + Esar -> DEsar
		X(ESAR) = X(ESAR) - 2;
		X(DESAR)= X(DESAR)+1;
		break;
	case 1:		// EsaR + Esar <- DEsar
		X(ESAR) = X(ESAR) + 2;
		X(DESAR) = X(DESAR) - 1;
		break;
	case 2:		//AHL + DEsar -> DEsar_AHL1
		X(DESAR) = X(DESAR) - 1;
		X(DESAR_AHL1) = X(DESAR_AHL1) - 1;
		break;
	case 3:		// AHL + DEsar <- DEsar_AHL1
		X(DESAR) = X(DESAR) - 1;
	    X(DESAR_AHL1) = X(DESAR_AHL1) - 1;
		break;
	case 4:		// AHL + DEsar_AHL1 -> DEsar_AHL
	    X(DESAR_AHL) = X(DESAR_AHL) + 1;
		X(DESAR_AHL1) = X(DESAR_AHL1) - 1;
		break;
	case 5:		// AHL + DEsar_AHL1 -> DEsar_AHL
		X(DESAR_AHL) = X(DESAR_AHL) - 1;
		X(DESAR_AHL1) = X(DESAR_AHL1) + 1;
		break;
	case 6:		// DESAR_AHL + Pfree -> PeSARAHL + AHL
		X(DESAR_AHL) = X(DESAR_AHL) - 1;
		X(PFREE) = X(PFREE) - 1;
		X(PDESAR_AHL) = X(PDESAR_AHL)+1;
		break;
	case 7:		// DESAR_AHL + Pfree <- PeSARAHL + AHL
		X(DESAR_AHL) = X(DESAR_AHL) - 1;
		X(PFREE) = X(PFREE) - 1;
		X(PDESAR_AHL) = X(PDESAR_AHL)+1;
		break;
	case 8:	  //PdeSAR_AHL -> PeSAR + AHL
		X(PDESAR_AHL) = X(PDESAR_AHL) - 1;
		X(PDESAR) = X(PDESAR) + 1;
		break;
	case 9:	  //Pdesar_AHL <- Pesar + AHL
		X(PDESAR_AHL) = X(PDESAR_AHL) + 1;
		X(PDESAR) = X(PDESAR) - 1;
		break;
	case 10:    //-> ESAR
		X(ESAR)=X(ESAR)+1;
		break;
	case 11:    // ESAR ->
		X(ESAR)=X(ESAR)-1;
		break;
	case 12:  //DEsaR ->
		X(DESAR)=X(DESAR)-1;
		break;
	case 13: //PFREE -> PFREE + mRNAsfgfp
		X(MRNA_GFP)=X(MRNA_GFP)+1;
		break;
	case 14: //mRNAsfgfp ->
		X(MRNA_GFP)=X(MRNA_GFP)-1;
		break;
	case 15: //mRNAsfgfp -> mRNAsfgfp + sfGFP
		X(GFP)=X(GFP)+1;
		break;
	case 16: // sfGFP ->
		X(GFP)=X(GFP)-1;
		break;
	}
}

void Esar_GFP_FlModel::_createOutputState(const state& X,
		const parameters& params, EiVectorRef output) {

	EiConstVectorRef mu = params.head(output_states.size());
	EiConstVectorRef std = params.block(output_states.size(), 0,
			output_states.size(), 1);
	_addFlLevel(X, mu, std, output);
}

void Esar_GFP_FlModel::_setInitialConditions(EiVectorRef X) {
	X(ESAR)= 0;
	X(DESAR)=0;
	X(DESAR_AHL1)=0;
	X(DESAR_AHL) = 0;
	X(PFREE) =15;
    X(PDESAR)=0;
	X(PDESAR_AHL) =0;
	X(MRNA_GFP) = 0;
	X(GFP) = 0;
}

}/* namespace INSIGHTv3 */




