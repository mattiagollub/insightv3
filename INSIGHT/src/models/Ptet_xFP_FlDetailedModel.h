/*
 * Ptet_xFP_FlDetailedModel.h
 *      Author: ETH iGEM 2016
 */

#ifndef Ptet_xFP_FlDetailedModel_H_
#define Ptet_xFP_FlDetailedModel_H_

#include "ModelFl.h"

#include <stddef.h>

namespace INSIGHTv3 {

class Ptet_xFP_FlDetailedModel: public INSIGHTv3::ModelFl {
public:
	Ptet_xFP_FlDetailedModel(
			const std::vector<EiVector> background_fluorescence, RngPtr r,
			std::vector<int> output_states);
	virtual ~Ptet_xFP_FlDetailedModel();
	void initializeTrajectories(const parameters& params);
	void initializeMeasurement(const parameters& params) {};

protected:
	void _updatePropensities(const state& X, const parameters& params,
			propensities* props) const;
	void _updateState(const size_t reaction_index, EiVectorRef X) const;
	void _createOutputState(const state& X, const parameters& params,
			EiVectorRef output);
	void _setInitialConditions(EiVectorRef X);

private:

	static const size_t _NUM_SPECIES = 3;
	static const size_t _NUM_PARAMETERS = 9;
	static const size_t _NUM_REACTIONS = 4;
	static const MODELS _MODEL = MODEL_PTET_XFP_DETAILED_FL;
	static const double NP = 15;

	DistUniform _dist_uniform;
	// DistNormal* _dist_normal;
	// DistLognormal* _dist_lognormal;
};

} /* namespace INSIGHTv3 */

#endif /* Ptet_xFP_FlDetailedModel_H_ */
