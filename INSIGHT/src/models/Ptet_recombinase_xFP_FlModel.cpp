/*
 * Ptet_recombinase_xFP_FlModel.h
 *      Author: ETH iGEM 2016
 */

#include <Ptet_recombinase_xFP_FlModel.h>
#include <string>
#include <cmath>

#include "INSIGHTv3.h"

namespace INSIGHTv3 {

Ptet_recombinase_xFP_FlModel::Ptet_recombinase_xFP_FlModel(
		const std::vector<EiVector> background_fluorescence, RngPtr r,
		std::vector<int> output_states) :
		ModelFl(background_fluorescence, r, _NUM_SPECIES,
				_NUM_PARAMETERS + 2 * output_states.size(), _NUM_REACTIONS,
				"aTc-inducible recombinase model for the ETH iGEM 2016 project.",
				_MODEL, output_states), _dist_uniform(*r), _prevDose(-1.0) {

}

Ptet_recombinase_xFP_FlModel::~Ptet_recombinase_xFP_FlModel() {
}

#define MRNA_INV 0
#define BXB1 9
#define DBXB1 2
#define S_0 3
#define S_1 4
#define S_2 5
#define POUT_FLIPPED 6
#define SF_0 7
#define MRNA_XFP 8
#define XFP 1

// #define PAR(id) std::pow(10.0, params(begin_index + id) - 10.0);
#define PAR(id) params(begin_index + id);

void Ptet_recombinase_xFP_FlModel::_updatePropensities(const state& X,
		const parameters& params, propensities* props) const {

	// Recover species from X vector
	double mRNAinv = X(MRNA_INV);
	double Bxb1 = X(BXB1);
	double DBxb1 = X(DBXB1);
	double S0 = X(S_0);
	double S1 = X(S_1);
	double S2 = X(S_2);
	double Pout_flipped = X(POUT_FLIPPED);
	double SF0 = X(SF_0);
	double mRNAxfp = X(MRNA_XFP);
	double xFP = X(XFP);

	// Recover parameters from params vector
	int begin_index = 2 * output_states.size(); // the first parameters are reserved for the mean and std of the fluourescence
	double l_Ptet 		= 0.057508;
	double n_aTc 		= 1.5698;
	double Km_aTc 		= params(begin_index);
	double k_mRNAinv 	= 0.38243;
	double d_mRNAinv 	= PAR(1);
	double k_Bxb1 		= PAR(2);
	double d_Bxb1 		= PAR(3);
	double d_DBxb1 		= PAR(11);
	double k_DBxb1 		= PAR(4);
	double k_DBxb1_		= PAR(5);
	double k_attBP 		= PAR(6);
	double k_attBP_ 	= k_attBP * 70.0;
	double k_attLR 		= PAR(7);
	double k_attLR_ 	= k_attLR * 15.0;
	double k_flip 		= PAR(8);
	double k_mRNAxfp 	= PAR(9);
	double d_mRNAxfp 	= 8.9323;
	double k_xFP 		= 0.012519;
	double d_xFP 		= 0.017748;

	// Evaluate the propensities
	double aTc = _aTc;
	double P_ON = l_Ptet + (1-l_Ptet)*pow(aTc, n_aTc)/(Km_aTc+pow(aTc, n_aTc));

	(*props)(0) = k_mRNAinv*P_ON*NP;				// P_ON -> P_ON + mRNAinv
	(*props)(1) = d_mRNAinv*mRNAinv;				// mRNAinv ->
	(*props)(2) = k_Bxb1*mRNAinv;					// mRNAinv -> mRNAinv + Bxb1
	(*props)(3) = d_Bxb1*Bxb1;						// Bxb1 ->
	(*props)(4) = k_DBxb1*Bxb1*(Bxb1-1)/2;			// 2Bxb1 -> DBxb1
	(*props)(5) = k_DBxb1_*DBxb1;					// DBxb1 -> 2Bxb1
	(*props)(6) = k_attBP*DBxb1*S0*2;				// S_0 + DBxb1 -> S_1
	(*props)(7) = k_attBP_*S1;						// S_1 -> S_0 + DBxb1
	(*props)(8) = k_attBP*DBxb1*S1;					// S_1 + DBxb1 -> S_2
	(*props)(9) = k_attBP_*S2*2;					// S_2 -> S_1 + DBxb1
	(*props)(10) = k_flip*S2; 						// S_2 -> Pout_flipped
	(*props)(11) = k_attLR*SF0*DBxb1;		 		// SF_0 + DBxb1 -> SF_1
	(*props)(12) = k_attLR_*(2*Pout_flipped-SF0);	// SF_1 -> SF_0 + DBxb1
	(*props)(13) = k_mRNAxfp*Pout_flipped;			// Pout_flipped -> Pout_flipped + mRNAxfp
	(*props)(14) = d_mRNAxfp*mRNAxfp;				// mRNAxfp ->
	(*props)(15) = k_xFP*mRNAxfp;					// mRNAxfp -> mRNAxfp + xFP
	(*props)(16) = d_xFP*xFP;						// xFP ->
	(*props)(17) = d_DBxb1*DBxb1;					// DBxb1 ->
}

void Ptet_recombinase_xFP_FlModel::_updateState(const size_t reaction_index,
		EiVectorRef X) const {

	// Update the state vector according to which reaction fired
	switch (reaction_index) {
		case 0:		X(MRNA_INV)++; break;						// P_ON -> P_ON + mRNAinv
		case 1:		X(MRNA_INV)--; break;						// mRNAinv ->
		case 2:		X(BXB1)++; break;							// mRNAinv -> mRNAinv + Bxb1
		case 3:		X(BXB1)--; break;							// Bxb1 ->
		case 4:		X(BXB1)-=2; X(DBXB1)++; break;				// 2Bxb1 -> DBxb1
		case 5:		X(BXB1)+=2; X(DBXB1)--; break;				// DBxb1 -> 2Bxb1
		case 6:		X(S_0)--; X(DBXB1)--; X(S_1)++; break;		// S_0 + DBxb1 -> S_1
		case 7:		X(S_0)++; X(DBXB1)++; X(S_1)--; break;		// S_1 -> S_0 + DBxb1
		case 8:		X(S_1)--; X(DBXB1)--; X(S_2)++; break;		// S_1 + DBxb1 -> S_2
		case 9:		X(S_1)++; X(DBXB1)++; X(S_2)--; break;		// S_2 -> S_1 + DBxb1
		case 10:	X(S_2)--; X(POUT_FLIPPED)++; break;			// S_2 -> Pout_flipped
		case 11:	X(SF_0)--; X(DBXB1)--; break;				// SF_0 + DBxb1 -> SF_1
		case 12:	X(SF_0)++; X(DBXB1)++; break;				// SF_1 -> SF_0 + DBxb1
		case 13:	X(MRNA_XFP)++; break;						// Pout_flipped -> Pout_flipped + mRNAxfp
		case 14:	X(MRNA_XFP)--; break;						// mRNAxfp ->
		case 15:	X(XFP)++; break;							// mRNAxfp -> mRNAxfp + xFP
		case 16:	X(XFP)--; break;							// xFP ->
		case 17:	X(DBXB1)--; break;							// DBxb1 ->
	}
}

void Ptet_recombinase_xFP_FlModel::_createOutputState(const state& X,
		const parameters& params, EiVectorRef output) {

	EiConstVectorRef mu = params.head(output_states.size());
	EiConstVectorRef std = params.block(output_states.size(), 0,
			output_states.size(), 1);
	state s = X.replicate(1, 1);
	if (_dist_uniform() < 0.09)
	 	s(XFP) = 0; // Dead cell
	_addFlLevel(s, mu, std, output);
}

void Ptet_recombinase_xFP_FlModel::_setInitialConditions(EiVectorRef X) {
	X(MRNA_INV) = 0;
	X(BXB1) = 0;
	X(DBXB1) = 0;
	X(S_0) = NP;
	X(S_1) = 0;
	X(S_2) = 0;
	X(POUT_FLIPPED) = 0;
	X(SF_0) = 0;
	X(MRNA_XFP) = 0;
	X(XFP) = 0;
}

void Ptet_recombinase_xFP_FlModel::initializeTrajectories(const parameters& params) {
	if (_prevDose != dose && false) {
		if (dose <= 0.0) {
			this->_aTc = 0.0;
		}
		else if (dose > 50) {
			this->_aTc = dose;
		}
		else {
			DistPoisson dist_poisson(*_r, PoissonMapping(dose));
			this->_aTc = dist_poisson();
		}
		_prevDose = dose;
	}
	else {
		this->_aTc = dose;
	}
}

}/* namespace INSIGHTv3 */
